<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style.css">

    <link rel="icon" type="images/png" href="media/images/login/siap.png">
</head>
<body>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">Daftar</h2>
                        <form method="POST" class="register-form" id="register-form" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="nama_lengkap" id="name" placeholder="Nama Lengkap" required/>
                            </div>
                            <div class="form-group">
                                <label for="username"><i class="zmdi zmdi-account-circle"></i></label>
                                <input type="text" name="username" id="username" placeholder="Nama Pengguna" required/>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="pass" placeholder="Kata Sandi" required/>
                            </div>
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Email Anda" required/>
                            </div>
                            <div class="form-group">
                                <label class="tempat_lahir"><i class="zmdi zmdi-pin-drop"></i></label>
                                <input type="text" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" required>
                            </div>
                            <div class="form-group">
                                <label for="date"><i class="zmdi zmdi-assignment-o"></i></label>
                                <input type="date" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" required>
                            </div>           
                            <div class="form-group">
                                <select name="jenis_kelamin">
                                    <option value="" disabled> Jenis Kelamin </option>
                                    <option value="Laki-laki" >Laki-laki</option>
                                    <option value="Perempuan" >Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="alamat"></label>
                                <textarea name="alamat" class="textarea" placeholder="Alamat" required></textarea>
                            </div>
                            <div class="form-group">
                                <label class=""></label>
                                <input type="file" name="foto_profil" id="" required>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Daftar"/>
                            </div>              
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="media/images/login/siap.png" alt="sing up image"></figure>
                        <a href="index.php" class="signup-image-link">Sudah Punya Akun ?</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- JS -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>