<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="media/images/home/favicon.png">
    <!-- Bootstrap CSS -->

    <!-- My css -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


        <link rel="stylesheet" href="assets/homeAssets/css/slick/slick.css"> 
        <link rel="stylesheet" href="assets/homeAssets/css/slick/slick-theme.css">
        <link rel="stylesheet" href="assets/homeAssets/css/animate.css">
        <link rel="stylesheet" href="assets/homeAssets/css/iconfont.css">
        <link rel="stylesheet" href="assets/homeAssets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/homeAssets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/homeAssets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/homeAssets/css/bootsnav.css">
        <link rel="stylesheet" href="assets/homeAssets/css/style.css">
        <link rel="stylesheet" href="assets/homeAssets/css/responsive.css" />
        <script src="assets/homeAssets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <title>Pendaftaran Prakerin Siswa</title>
  </head>
  <body data-spy="scroll" data-target=".navbar-collapse">
    <!-- navbar -->
    <nav class="navbar transparent navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://inovindoweb.com/">
                        <img src="media/images/home/logo.png" class="logo" alt="">
                    </a>
                    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#home">Home</a></li>
        <li><a href="#lokasi">Lokasi</a></li>
        <li><a href="#tentang">Tentang Kami</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span>&nbspProfile<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Pemberitahuan</a></li>
            <li><a href="#">Pengaturan</a></li>
            <li class="divider"></li>
            <li><a href="logout.php">Log Out</a></li>
          </ul>
        </li>
      </ul>
  </div>
</div>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



    <div class="container col koki">
        <!-- Sign up form -->
            <div class="container box">
              <div class="col">
                <div class="container">
                  <form action="tambahSiswa.php" method="post" enctype="multipart/form-data"> <br> <br>
                                    <div class="row">
                                      <h2 class="col text-center">Keterangan Siswa</h2>
                                    </div>
                                           <br>
                                          <input name="nama_lengkap" required class="form-control" type="text" id="nama" autocomplete="off" placeholder="Nama Lengkap"><br>
                                          <input name="nis" required class="form-control" type="text" id="nisn" autocomplete="off" placeholder="NIS"><br>

                                            <div class="row">
                                              <div class="col">
                                                <input name="tempat_lahir" class="form-control" required  type="text"  id="tempat_lahir" autocomplete="off" placeholder="Tempat Lahir">
                                              </div>
                                              <div class="col">
                                                <input name="tanggal_lahir" class="form-control" required  type="date"  id="tanggal_lahir" autocomplete="off">
                                              </div>
                                            </div>
                                          <br>
                                          <select name="jenis_kelamin" required class="form-control" id="jenis_kelamin">
                                                <option value="">Jenis Kelamin</option>
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                            <br>
                                          <select name="agama" required class="form-control" id="Agama">
                                                <option value="">Agama</option>
                                                <option value="Islam">Islam</option>
                                                <option value="Kristen">Kristen</option>
                                                <option value="Hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                          </select>
                                                  <br>
                                          <textarea name="alamat_rumah" required class="form-control" rows="3" type="text" id="alamat" cols="50" rows="5" placeholder="Alamat Rumah"></textarea><br>
                                          <select name="kelas" required class="form-control" id="Kelas">
                                                <option value="">Kelas</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                          </select><br>
                                          <input name="jurusan" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="Jurusan"></td><br>
                                          <input name="email" required class="form-control" type="email" id="prodi" size="30px" autocomplete="off" placeholder="Email"></td><br>
                                          <input name="no_telp" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="No.telp (0813333333)"></td><br>
                                          <input name="kd_sekolah" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="Kode Sekolah"><p style="font-size: 12px">Belum tau Kode Sekolahmu? cari disini :<a href="http://sekolah.data.kemdikbud.go.id/" target="__blank">http://sekolah.data.kemdikbud.go.id/</a></td><br><br/>
                                          <input name="nama_sekolah" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="Asal Sekolah"><br/>
                                          <textarea name="alamat_sekolah" required class="form-control" rows="3" type="text" id="alamat" cols="50" rows="5" placeholder="Alamat Sekolah"></textarea><br>
                                          
                                          <div class="row">
                                           <div class="col text-center">
                                             <h3>Tanggal PKL</h3>
                                           </div>
                                          </div>
                                            <div class="row">
                                              <div class="col">
                                                <strong>Tanggal Masuk</strong>
                                                <input name="dari_tgl" class="form-control" required  type="date"  autocomplete="off">
                                              </div>
                                              <div class="col">
                                                <strong>Tanggal Keluar</strong>
                                                <input name="sampai_tgl" class="form-control" required  type="date"  autocomplete="off">
                                              </div>
                                            </div>

                                            <br>
                                          <strong><p>Masukan pas foto 4x4</p></strong>
                                          <input type="file" name="foto" class="form-control"> <br>
                                          <!-- syarat modal -->
                                          <div class="custom-control custom-checkbox">
                                            <input type="checkbox" required class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">Saya menyetujui <a href="#" data-toggle="modal" data-target="#exampleModalLong">Syarat & Ketentuan</a> yang berlaku</label>
                                          </div>
                                          <br>
                                          <!-- Modal -->
                                          <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLongTitle">Syarat & Ketentuan</h5>
                                                </div>
                                                <div class="modal-body">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br> <br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>


                                          <div class="group-button">
                                            <div class="row">
                                              <div class="col">
                                                <button type="reset" class="btn btn-primary">
                                                  Reset
                                                </button>
                                              </div>
                                              <div class="row">
                                                <div class="col">
                                                  <!-- Button trigger modal -->
                                                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                    Daftar
                                                  </button>

                                                  <!-- Modal -->
                                                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                      <div class="modal-content">
                                                        <div class="modal-header">

                                                          <h5 class="modal-title" id="myModalLabel">Pastikan data yang anda masukan benar!</h5>
                                                        </div>
                                                        <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                                                          <button type="submit" class="btn btn-primary">Ya</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>

                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                            </div>
                                          </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/homeAssets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/homeAssets/js/vendor/bootstrap.min.js"></script>
        <script src="https:/js/google_map.js"></script>
        <script src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>

        <script src="assets/homeAssets/js/owl.carousel.min.js"></script>
        <script src="assets/homeAssets/js/jquery.magnific-popup.js"></script>
        <script src="assets/homeAssets/js/jquery.easing.1.3.js"></script>
        <script src="assets/homeAssets/css/slick/slick.js"></script>
        <script src="assets/homeAssets/css/slick/slick.min.js"></script>
        <script src="assets/homeAssets/js/jquery.collapse.js"></script>
        <script src="assets/homeAssets/js/bootsnav.js"></script>



        <script src="assets/homeAssets/js/plugins.js"></script>
        <script src="assets/homeAssets/js/main.js"></script>

        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- countTo -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Magnific Popup -->
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <!-- Main -->
        <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
