<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- My css -->
    <link rel="stylesheet" href="assets/css/style-bezy.css">

    <title>Pendaftaran Prakerin Siswa</title>
  </head>
  <body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li>
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="DaftarSiswa.html">Pendaftaran Prakerin Siswa</a>
      </li>
      <li class="nav-item nav-item active">
        <a class="nav-link" href="DaftarMahasiswa.php">Pendaftaran Prakerin Mahasiswa</a>
      </li>
    </ul>
  </div>
</nav>


    <div class="container col main">
        <!-- Sign up form -->
            <div class="container box">
              <div class="col">
                <div class="container">
                  <form action="tambahMSiswa.php" method="post" enctype="multipart/form-data"> <br> <br>
                                    <div class="row">
                                      <h2 class="col text-center">Keterangan Mahasiswa</h2>
                                    </div>
                                           <br>
                                          <input name="nama_lengkap" required class="form-control" type="text" id="nama" autocomplete="off" placeholder="Nama Lengkap"><br>
                                          <input name="nim" required class="form-control" type="text" id="nisn" autocomplete="off" placeholder="NIM"><br>

                                            <div class="row">
                                              <div class="col">
                                                <input name="tempat_lahir" class="form-control" required  type="text"  id="tempat_lahir" autocomplete="off" placeholder="Tempat Lahir">
                                              </div>
                                              <div class="col">
                                                <input name="tanggal_lahir" class="form-control" required  type="date"  id="tanggal_lahir" autocomplete="off">
                                              </div>
                                            </div>
                                          <br>
                                          <select name="jenis_kelamin" required class="form-control" id="jenis_kelamin">
                                                <option value="">Jenis Kelamin</option>
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                          </select>
                                            <br>
                                          <select name="agama" required class="form-control" id="Agama">
                                                <option value="">Agama</option>
                                                <option value="Islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="Hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                          </select>
                                                  <br>
                                          <textarea name="alamat_rumah" required class="form-control" rows="3" type="text" id="alamat" cols="50" rows="5" placeholder="Alamat Rumah"></textarea><br>
                                          <input name="jurusan" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="Jurusan"></td><br>
                                          <input name="email" required class="form-control" type="email" id="prodi" size="30px" autocomplete="off" placeholder="Email"></td><br>
                                          <input name="no_telp" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="No.telp (0813333333)"></td><br>
                                          <input name="kd_universitas" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="Kode Universitas"></td><br>
                                          <input name="nama_universitas" required class="form-control" type="text" id="jurusan" size="30px" autocomplete="off" placeholder="Asal Universitas"><br/>
                                          <textarea name="alamat_kampus" required class="form-control" rows="3" type="text" id="alamat" cols="50" rows="5" placeholder="Alamat Rumah"></textarea><br>
                                          <div class="row">
                                           <div class="col text-center">
                                             <h3>Tanggal PKL</h3>
                                           </div>
                                          </div>
                                            <div class="row">
                                              <div class="col">
                                                <strong>Tanggal Masuk</strong>
                                                <input name="dari_tgl" class="form-control" required  type="date"  autocomplete="off">
                                              </div>
                                              <div class="col">
                                                <strong>Tanggal Keluar</strong>
                                                <input name="sampai_tgl" class="form-control" required  type="date"  autocomplete="off">
                                              </div>
                                            </div>

                                            <br>
                                          <strong><p>Masukan pas foto 4x4</p></strong>
                                          <input type="file" name="foto" class="form-control" id="exampleInputFile" accept="image/x-png,image/gif,image/jpeg"> <br>
                                          <!-- syarat modal -->
                                          <div class="custom-control custom-checkbox">
                                            <input type="checkbox" required class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">Saya menyetujui <a href="#" data-toggle="modal" data-target="#exampleModalLong">Syarat & Ketentuan</a> yang berlaku</label>
                                          </div>
                                          <br>
                                          <!-- Modal -->
                                          <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLongTitle">Syarat & Ketentuan</h5>
                                                </div>
                                                <div class="modal-body">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br> <br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>


                                          <div class="group-button">
                                            <div class="row">
                                              <div class="col">
                                                <button type="reset" class="btn btn-primary">
                                                  Reset
                                                </button>
                                              </div>
                                              <div class="row">
                                                <div class="col">
                                                  <!-- Button trigger modal -->
                                                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                    Daftar
                                                  </button>

                                                  <!-- Modal -->
                                                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                      <div class="modal-content">
                                                        <div class="modal-header">

                                                          <h5 class="modal-title" id="myModalLabel">Pastikan data yang anda masukan benar!</h5>
                                                        </div>
                                                        <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                                                          <button type="submit" class="btn btn-primary">Ya</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>

                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                            </div>
                                          </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
