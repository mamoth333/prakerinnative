<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="5; url=home.php" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- My css -->
    <link rel="stylesheet" href="assets/css/style-bezy.css">

    <title>Pendaftaran Prakerin Siswa</title>

    <!--  -->
    <script>
function myFunction() {
    window.location="home.php";
}
</script>

  </head>

  <body class="warna" onclick="setTimeout(myFunction, 2000);">




    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li>
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item nav-item active">
        <a class="nav-link" href="#">Pendaftaran Prakerin</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Dan Lain Lain</a>
      </li>
    </ul>
  </div>
</nav>
<!--  nav  -->


    <div class="container col main">
        <!-- Sign up form -->
            <div class="container box">
              <div class="col">
                <div class="container">
                  <br>
                    <h4 class="col text-center">Selamat <?php echo $_SESSION["nama_lengkapl"]; ?> data berhasil dikirim</h4>
                    <h4 class="col text-center">Cek email anda secara berkala untuk informasi lebih lanjut <a href="https://www.google.com/gmail/" target="_blank"> <?php echo $_SESSION["email"]; ?> </a></h4>

                  <br>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
