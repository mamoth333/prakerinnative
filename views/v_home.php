<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>INOVINDO WEB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="media/images/home/favicon.png">

        <!--Google Font link-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


        <link rel="stylesheet" href="assets/homeAssets/css/slick/slick.css"> 
        <link rel="stylesheet" href="assets/homeAssets/css/slick/slick-theme.css">
        <link rel="stylesheet" href="assets/homeAssets/css/animate.css">
        <link rel="stylesheet" href="assets/homeAssets/css/iconfont.css">
        <link rel="stylesheet" href="assets/homeAssets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/homeAssets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/homeAssets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/homeAssets/css/bootsnav.css">

        <!-- xsslider slider css -->


        <!--<link rel="stylesheet" href="assets/homeAssets/css/xsslider.css">-->




        <!--For Plugins external css-->
        <!--<link rel="stylesheet" href="assets/homeAssets/css/plugins.css" />-->

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/homeAssets/css/style.css">
        <!--<link rel="stylesheet" href="assets/homeAssets/css/colors/maron.css">-->

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/homeAssets/css/responsive.css" />

        <script src="assets/homeAssets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".navbar-collapse">


    

   <nav class="navbar transparent navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://inovindoweb.com/">
                        <img src="media/images/home/logo.png" class="logo" alt="">
                    </a>
                    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#home">Home</a></li>
        <li><a href="#lokasi">Lokasi</a></li>
        <li><a href="#tentang">Tentang Kami</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span>&nbspProfile<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Pemberitahuan</a></li>
            <li><a href="#">Pengaturan</a></li>
            <li class="divider"></li>
            <li><a href="logout.php">Log Out</a></li>
          </ul>
        </li>
      </ul>
  </div>
</div>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

 <!--Home Sections-->

            <section id="home" class="home bg-black fix">
                <div class="container">
                    <div class="row">
                        <div class="main_home text-center">
                            <div class="col-md-12">
                                <div class="hello_slid">
                                    <div class="slid_item">
                                </div>
                            </div>
                        </div>
                    </div><!--End off row-->
                </div><!--End off container -->
            </div>
            </section> <!--End off Home Sections-->


    <section class="feature">
        <div class="main_prod uct roomy-80">
        <div id="gtco-features-3">
        <div class="gtco-container">
            <div class="gtco-flex ">
                <div class="feature feature-1 animate-box " data-animate-effect="fadeInUp" style="background-color: #f44b42">
                    <div class="feature-inner">
                        <span class="icon">
                            <i class="ti-search"></i>
                        </span>
                        <h3>Pendaftaran</h3>
                        <br><br><br>

                        <p>Silahkan Klik Di sini Untuk Mendaftarkan Diri Anda!</p>
                        <br>
                        <p><a href="tambahSiswa.php" class="btn btn-white btn-outline">Siswa</a>&nbsp &nbsp<a href="tambahMSiswa.php" class="btn btn-white btn-outline">Mahasiswa</a></p>

                    </div>
                </div>
                <div class="feature feature-1 animate-box" data-animate-effect="fadeInUp" style="background-color: #eead0e"> 
                    <div class="feature-inner">
                        <span class="icon">
                            <i class="ti-announcement"></i>
                        </span>
                        <h3>Data Pendaftaran</h3>
                        <br><br><br>
                        <p>Lihat Hasil Pendaftaran Anda, Apakah Anda Sudah Terdaftar Atau Belum?</p>
                        <p><a href="#" class="btn btn-white btn-outline">Lihat!</a></p>
                    </div>
                </div>
                <div class="feature feature-1 animate-box" data-animate-effect="fadeInUp" style="background-color: #3b90f1">
                    <div class="feature-inner">
                        <span class="icon">
                            <i class="ti-timer"></i>
                        </span>
                        <h3>Jadwal</h3>
                        <br><br><br>
                        <p>Lihat Jadwal Ketersediaan Waktu Prakerin</p>
                        <br>
                        <p><a href="#" class="btn btn-white btn-outline">Lihat!</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    </section>

            <!-- map Section -->
            <section id="lokasi" class="feature">
                  <div class="container">
                    <div class="main_product roomy-80">
                        <div class="head_title text-center fix"style="background-color:#e7e7e7">
                            <div id="map-container" class="z-depth-1">
                                 <h1 lass="text-uppercase">Lokasi INOVINDO WEB</h1>
                                 <div id="map" class="map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63370.62065870709!2d107.61396953594088!3d-6.930728698315466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2e9d823378f26b24!2sINOVINDO+WEB+%7C+JASA+PEMBUATAN+WEBSITE+PROFESIONAL!5e0!3m2!1sid!2sid!4v1538795056460" width="300" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>&nbsp&nbsp
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63370.62065870709!2d107.61396953594088!3d-6.930728698315466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddddeee9ed9731a1!2sINOVINDO+GROUP!5e0!3m2!1sid!2sid!4v1538795003018" width="350" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>&nbsp &nbsp
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126727.35139591085!2d107.60340300211023!3d-6.982194779024858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7080a6c3fb9%3A0xe172ca4a496ebec8!2sPT.+Inovindo+Digital+Media!5e0!3m2!1sid!2sid!4v1539399419279" width="350" height="325" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off map Section-->
                           

            <footer id="tentang" class="footer action-lage bg-white p-top-80">
                <!--<div class="action-lage"></div>-->
                <div class="container">
                    <div class="row">
                        <div class="widget_area">
                            <div class="col-md-3">
                                <div class="widget_item widget_about">
                                    <h5 class="text-black">Tentang Kami</h5>
                                    <div class="widget_ab_item m-top-30">
                                        <div class="item_icon"><i class="fa fa-location-arrow"></i></div>
                                        <div class="widget_ab_item_text">
                                            <p>Komplek Buana Citra Ciwastra C, Jl. Batusari No.27, Buahbatu, Bojongsoang, Bandung, Jawa Barat 40287</p>
                                        </div><br><br>
                                        <div class="item_icon"><i class="fa fa-location-arrow"></i></div>
                                        <div class="widget_ab_item_text">
                                            <p>blok b9, 10, Jalan PHH Mustofa Nomor No.39 Graha Sinergy Lantai 3, Komplek Surapati Core, Pasirlayung, Cibeunying Kidul, Kota Bandung, Jawa Barat 40192</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-30">
                                        <div class="item_icon"><i class="fa fa-phone"></i></div>
                                        <div class="widget_ab_item_text">
                                            <p> +628562251196</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-30">
                                        <div class="item_icon"><i class="fa fa-envelope-o"></i></div>
                                        <div class="widget_ab_item_text">
                                            <p>inovindocorp@gmail.com</p>
                                        </div>
                                    </div>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->
                            <div class="col-md-8">
                                <div class="widget_item widget_about">
                                    <img src="media/images/home/logo.png" class="logo" alt="">
                                        <div class="widget_ab_item_text">
                                            <p>INOVINDO berdiri pada tanggal 10 November 2011 di Bandung dengan Akta Pendirian No. 2 oleh Notaris Wilman Kusumajaya, SH., MKN. Merupakan perusahaan Jasa Pembuatan Website Profesional, memiliki tenaga ahli muda yang berkompeten dan senantiasa mengutamakan pelayanan yang prima bagi klien / partner perusahaan. <br><br>Saat ini sudah lebih dari 1000 perusahaan mempercayakan pembuatan website di INOVINDO. <br><br>Komunitas dan Asosiasi :</p>
                                                    
                                                    <li>HIPMI (Himpunan Pengusaha Muda Indonesia)</li>
                                                    <li>JCI (Junior Chamber International)</li>
                                                    <li>AMA (Asosiasi Manajemen Indonesia)</li>
                                                    <li>TDA (Tangan Di Atas)</li>
                                                    <li>IIBF (Indonesia Islamic Business Forum)</li>
                                                    <li>JPMI (Jaringan Pengusaha Muslim Indonesia)</li>
                                                    <li>PPA (Pola Pertolongan Allah)</li>
                                    </div>
                                </div>
                            </div><!-- End off col-md-3 -->
                        </div>
                    </div>
                </div>
                <div class="main_footer fix bg-mega text-center p-top-40 p-bottom-30 m-top-80">
                    <div class="col-md-12">
                        <p class="wow fadeInRight" data-wow-duration="1s">
                            Copyright ©
                            <a href="http://inovindoweb.com/" style="color: #000;">INOVINDO WEB</a>
                            2018. All right reserved
                        </p>
                    </div>
                </div>
            </footer>




        </div>

        <!-- JS includes -->

        <script src="assets/homeAssets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/homeAssets/js/vendor/bootstrap.min.js"></script>
        <script src="https:/js/google_map.js"></script>
        <script src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>

        <script src="assets/homeAssets/js/owl.carousel.min.js"></script>
        <script src="assets/homeAssets/js/jquery.magnific-popup.js"></script>
        <script src="assets/homeAssets/js/jquery.easing.1.3.js"></script>
        <script src="assets/homeAssets/css/slick/slick.js"></script>
        <script src="assets/homeAssets/css/slick/slick.min.js"></script>
        <script src="assets/homeAssets/js/jquery.collapse.js"></script>
        <script src="assets/homeAssets/js/bootsnav.js"></script>



        <script src="assets/homeAssets/js/plugins.js"></script>
        <script src="assets/homeAssets/js/main.js"></script>

        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- countTo -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Magnific Popup -->
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <!-- Main -->
        <script src="js/main.js"></script>

    </body>
</html>
